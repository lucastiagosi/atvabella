package Arraylist;
public class Arraylist {
	private static final int TAM_INICIAL = 3;
	private int pos;
	private double[] valores = new double[TAM_INICIAL];

	/**
	 * Este metodo adiciona valores ao array, tratando-o quando estourar.
	 * 
	 * @author luccastiago
	 * @param valor
	 */

	public void addValor(double valor) {

		if (valores.length == pos) {
			double[] arraytemp = new double[TAM_INICIAL * valores.length];
			for (int i = 0; i < valores.length; i++) {
				arraytemp[i] = valores[i];
			}
			valores = arraytemp;
		}
		valores[pos] = valor;
		pos++;
	}

	/**
	 * Este m�todo remove um valor do array, caso ele exista.
	 * 
	 * @param valor
	 */

	public void removerValor(double valor) throws ValorInvalidoException {
		boolean verificacao = false;
		for (int i = 0; i < valores.length; i++) {
			if (valor == valores[i]) {
				valores[i] = (Double) null;
				verificacao = true;
			}
		}
		if (verificacao == false) {
			throw new ValorInvalidoException();
		}
	}

	/**
	 * Este metodo retorna o tamanho do array.
	 */
	public int getTamanhoArray() {
		return pos;
	}

	/**
	 * Este m�todo retorna os elementos do array.
	 */
	public double[] getArray() {
		return valores;
	}
	
	/**
	 * Este metodo retorna a quantidade de elementos.
	 */
	public int qtdElementos() 
	{
		return pos + 1;
	}
	
	public double getValorPosicao() 
	{
		return valores[TAM_INICIAL] ;
	}

}
