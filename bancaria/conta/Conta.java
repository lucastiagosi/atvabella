package bancaria.conta;

public abstract class Conta {
	private int agencia;
	private int conta;
	private String titular;
	protected double limite;
	protected double saldo;
	protected double valorLimite;
	//CONSTRUTORES - SOBRECARGA
	public Conta(int agencia, int conta, String titular) {

		this.agencia = agencia;
		this.conta = conta;
		this.titular = titular;
	}

	public Conta(double saldo, double limite, double valorLimite) {

		this.saldo = saldo;
		this.limite = limite;		
		this.valorLimite = valorLimite;
	}

	public Conta(double saldo) {
		super();
		this.saldo = saldo;
	}
	//GETTERS
	public double getSaldo() 
	{
		return saldo;
	}
	
	public double getLimite() 
	{
		return limite;
	}
	
	public double getValorLimite() 
	{
		return valorLimite;
	}
	
	//M�TODO SACAR E DEPOSITAR
	public void sacar(double valor) 
	{
		saldo -= valor;
	}
	
	public void depositar(double valor) 
	{
		saldo += valor;
	}
	
}
