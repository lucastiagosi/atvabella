package bancaria.conta;

public class ContaCorrente extends Conta 
{
	public ContaCorrente(double saldo, double limite, double valorLimite) 
	{
		super(saldo, limite, valorLimite);
	}
	
	public void sacar(double valor) 
	{
		if(valor > saldo) 
		{
			double valorParcial = saldo - valor;
			saldo = 0;
			limite += valorParcial;
		}
		else 
		{
			saldo -= valor;
		}
	}
}
