package bancaria.conta;

public class ContaPoupanca extends Conta {
	private static final double JUROS = 0.3 ;
	
	public ContaPoupanca(double saldo) 
	{
		super(saldo);
	}

	public void depositar(double valor) 
	{
		saldo += valor + (valor * JUROS);
	}
}
