package Test.JUnit;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import bancaria.conta.ContaPoupanca;

class ContaPoupancaTest {
	ContaPoupanca c;
	
	
	@BeforeEach
	void setUp() throws Exception 
	{
		c = new ContaPoupanca(200);
	}

	@AfterEach
	void tearDown() throws Exception 
	{
		c = null;
	}

	@Test
	void depositarTest() {
		c.depositar(300);
		assertEquals(590, c.getSaldo());
		
	}

}
