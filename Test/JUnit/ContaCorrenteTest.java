package Test.JUnit;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import bancaria.conta.ContaCorrente;

class ContaCorrenteTest {
	ContaCorrente c;
	
	@BeforeEach
	void setUp() throws Exception 
	{
		c = new ContaCorrente(100, 2000, 2000);
	}
		
	@AfterEach
	void tearDown() throws Exception 
	{
		c = null;
	}

	@Test
	void sacarTest () {
		c.sacar(300);
		assertEquals(1800, c.getLimite());
		assertEquals(0, c.getSaldo());
		assertEquals(2000, c.getValorLimite());
	}

}
